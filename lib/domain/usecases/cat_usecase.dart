import 'package:catalogue/data/models/cat_item_model.dart';
import 'package:catalogue/domain/repositories/cat_repository.dart';

class CatUseCase {
  final CatRepository catRepository;

  CatUseCase({required this.catRepository});

  Future<CatListModel> getCats([bool forceRemote = false]) async {
    return catRepository.getCats(forceRemote);
  }

  Future<CatItemModel?> getCat(String id) => catRepository.getCat(id);
}
