import 'package:catalogue/data/models/cat_item_model.dart';

abstract class CatRepository {
  Future<CatListModel> getCats([bool forceRemote = false]);

  Future<CatItemModel?> getCat(String id);
}