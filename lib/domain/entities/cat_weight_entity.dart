import 'package:hive/hive.dart';

class CatWeightEntity {
  @HiveField(1)
  String? imperial;

  @HiveField(2)
  String? metric;

  CatWeightEntity({this.imperial, this.metric});
}
