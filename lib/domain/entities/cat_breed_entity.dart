import 'package:catalogue/domain/entities/cat_weight_entity.dart';
import 'package:hive/hive.dart';

class CatBreedEntity {
  @HiveField(1)
  String? id;

  @HiveField(2)
  String? name;

  @HiveField(3)
  String? cfaUrl;

  @HiveField(4)
  String? vetstreetUrl;

  @HiveField(5)
  String? vcahospitalsUrl;

  @HiveField(6)
  String? temperament;

  @HiveField(7)
  String? origin;

  @HiveField(8)
  String? countryCodes;

  @HiveField(9)
  String? countryCode;

  @HiveField(10)
  String? description;

  @HiveField(11)
  String? lifeSpan;

  @HiveField(12)
  int? indoor;

  @HiveField(13)
  int? lap;

  @HiveField(14)
  String? altNames;

  @HiveField(15)
  int? adaptability;

  @HiveField(16)
  int? affectionLevel;

  @HiveField(17)
  int? childFriendly;

  @HiveField(18)
  int? catFriendly;

  @HiveField(19)
  int? dogFriendly;

  @HiveField(20)
  int? energyLevel;

  @HiveField(21)
  int? grooming;

  @HiveField(22)
  int? healthIssues;

  @HiveField(23)
  int? intelligence;

  @HiveField(24)
  int? sheddingLevel;

  @HiveField(25)
  int? socialNeeds;

  @HiveField(26)
  int? strangerFriendly;

  @HiveField(27)
  int? vocalisation;

  @HiveField(28)
  int? experimental;

  @HiveField(29)
  int? hairless;

  @HiveField(30)
  int? natural;

  @HiveField(31)
  int? rare;

  @HiveField(32)
  int? rex;

  @HiveField(33)
  int? suppressedTail;

  @HiveField(34)
  int? shortLegs;

  @HiveField(35)
  String? wikipediaUrl;

  @HiveField(36)
  int? hypoallergenic;

  @HiveField(37)
  String? referenceImageId;

  CatWeightEntity? weight;

  CatBreedEntity({
    this.weight,
    this.id,
    this.name,
    this.cfaUrl,
    this.vetstreetUrl,
    this.vcahospitalsUrl,
    this.temperament,
    this.origin,
    this.countryCodes,
    this.countryCode,
    this.description,
    this.lifeSpan,
    this.indoor,
    this.lap,
    this.altNames,
    this.adaptability,
    this.affectionLevel,
    this.childFriendly,
    this.catFriendly,
    this.dogFriendly,
    this.energyLevel,
    this.grooming,
    this.healthIssues,
    this.intelligence,
    this.sheddingLevel,
    this.socialNeeds,
    this.strangerFriendly,
    this.vocalisation,
    this.experimental,
    this.hairless,
    this.natural,
    this.rare,
    this.rex,
    this.suppressedTail,
    this.shortLegs,
    this.wikipediaUrl,
    this.hypoallergenic,
    this.referenceImageId,
  });
}
