import 'package:catalogue/domain/entities/cat_breed_entity.dart';
import 'package:hive/hive.dart';

class CatItemEntity {
  @HiveField(1)
  String? id;

  @HiveField(2)
  String? url;

  @HiveField(3)
  int? width;

  @HiveField(4)
  int? height;

  List<CatBreedEntity>? breeds;

  CatItemEntity({
    this.id,
    this.url,
    this.width,
    this.height,
    this.breeds,
  });
}

typedef CatListEntity = List<CatItemEntity>;
