import 'package:catalogue/domain/entities/cat_weight_entity.dart';

class CatWeightModel extends CatWeightEntity {
  CatWeightModel({super.imperial, super.metric});

  CatWeightModel.fromJson(Map<String, dynamic> json) {
    imperial = json['imperial'];
    metric = json['metric'];
  }
}
