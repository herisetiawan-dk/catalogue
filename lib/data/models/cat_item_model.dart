import 'package:catalogue/data/models/cat_breed_model.dart';
import 'package:catalogue/domain/entities/cat_item_entity.dart';

class CatItemModel extends CatItemEntity {
  @override
  List<CatBreedModel>? breedsModel;

  CatItemModel({
    super.id,
    super.url,
    super.width,
    super.height,
    List<CatBreedModel>? breeds,
  }) : super(breeds: breeds) {
    breedsModel = breeds;
  }

  static CatItemModel fromJson(Map<String, dynamic> json) {
    final List<CatBreedModel> breeds = [];
    if (json['breeds'] != null) {
      for (dynamic breed in json['breeds']) {
        breeds.add(CatBreedModel.fromJson(breed));
      }
    }

    return CatItemModel(
      id: json['id'],
      url: json['url'],
      width: json['width'],
      height: json['height'],
      breeds: breeds,
    );
  }

  static CatListModel fromListJson(List<dynamic> listJson) {
    return listJson.map((cat) => CatItemModel.fromJson(cat)).toList();
  }
}

typedef CatListModel = List<CatItemModel>;
