import 'dart:convert';

import 'package:catalogue/common/constants/configuration_constants.dart';
import 'package:catalogue/data/models/cat_item_model.dart';
import 'package:http/http.dart' as http;

class CatRemoteDatasource {
  final _client = http.Client();

  Future<CatListModel> getCats([int page = 0]) async {
    final uri = Uri.parse(
        'https://api.thecatapi.com/v1/images/search?limit=20&has_breeds=1&page=$page');
    final response = await _client.get(uri, headers: {
      'Content-Type': 'application/json',
      'x-api-key': ConfigurationConstants.apiKey
    });

    final responseBody = jsonDecode(response.body);

    return CatItemModel.fromListJson(responseBody);
  }
}
