import 'package:catalogue/data/datasources/local/utils/database_utils.dart';
import 'package:hive/hive.dart';

abstract class BaseLocalDatasource<TableType, ModelType> {
  final String _boxName;
  late Future<Box<TableType>> _boxInstance;

  BaseLocalDatasource({
    required String boxName,
  }) : _boxName = boxName {
    _init();
  }

  void _init() {
    _boxInstance = DatabaseUtil.getBox<TableType>(boxName: _boxName);
  }

  Future<Box<TableType>> get boxInstance => _boxInstance;

  String get boxName => _boxName;

  Future<Box<TableType>?> get getBoxInstance async => _openBox();

  Future<List<ModelType>> getFormattedData();

  Future<void> insertOrUpdateItems(List<ModelType> items);

  Future<Box<TableType>?> _openBox() async {
    Box<TableType>? box = await boxInstance;
    return box;
  }

  Future<TableType?> get(String key) async {
    final Box<TableType>? box = await _openBox();
    return box?.get(key);
  }

  Future<List<TableType>> getAll() async {
    final Box<TableType>? box = await _openBox();
    return box!.toMap().values.toList();
  }

  Future<void> put(String key, TableType value) async {
    final Box<TableType>? box = await _openBox();
    await box!.put(key, value);
  }

  Future<void> putAll(Map<String, TableType> items) async {
    final Box<TableType>? box = await _openBox();
    await box!.putAll(items);
  }

  Future<void> delete(String key) async {
    final Box<TableType>? box = await _openBox();
    await box!.delete(key);
  }

  Future<void> deleteAll() async {
    final Box<TableType>? box = await _openBox();
    await box!.clear();
  }

  Future<void> deleteByKeys(List<String> keys) async {
    final Box<TableType>? box = await _openBox();
    await box!.deleteAll(keys);
  }

  Future<List<String>> get keys async {
    final Box<TableType>? box = await _openBox();
    final List<String> result = box!.keys.map((k) => k.toString()).toList();
    return result;
  }
}
