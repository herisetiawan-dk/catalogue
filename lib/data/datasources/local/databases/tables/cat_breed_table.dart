import 'package:catalogue/common/constants/local_database_type_constants.dart';
import 'package:catalogue/data/datasources/local/databases/tables/cat_weight_table.dart';
import 'package:catalogue/data/models/cat_breed_model.dart';
import 'package:hive/hive.dart';

part 'cat_breed_table.g.dart';

@HiveType(typeId: LocalDatabaseTypeConstants.catBreedTableId)
class CatBreedTable extends CatBreedModel {
  @HiveField(38)
  CatWeightTable? weightTable;

  CatBreedTable({
    super.id,
    super.name,
    super.cfaUrl,
    super.vetstreetUrl,
    super.vcahospitalsUrl,
    super.temperament,
    super.origin,
    super.countryCodes,
    super.countryCode,
    super.description,
    super.lifeSpan,
    super.indoor,
    super.lap,
    super.altNames,
    super.adaptability,
    super.affectionLevel,
    super.childFriendly,
    super.catFriendly,
    super.dogFriendly,
    super.energyLevel,
    super.grooming,
    super.healthIssues,
    super.intelligence,
    super.sheddingLevel,
    super.socialNeeds,
    super.strangerFriendly,
    super.vocalisation,
    super.experimental,
    super.hairless,
    super.natural,
    super.rare,
    super.rex,
    super.suppressedTail,
    super.shortLegs,
    super.wikipediaUrl,
    super.hypoallergenic,
    super.referenceImageId,
    this.weightTable,
  }) : super(weight: weightTable);

  static CatBreedTable fromModel(CatBreedModel model) => CatBreedTable(
        id: model.id,
        name: model.name,
        cfaUrl: model.cfaUrl,
        vetstreetUrl: model.vetstreetUrl,
        vcahospitalsUrl: model.vcahospitalsUrl,
        temperament: model.temperament,
        origin: model.origin,
        countryCodes: model.countryCodes,
        countryCode: model.countryCode,
        description: model.description,
        lifeSpan: model.lifeSpan,
        indoor: model.indoor,
        lap: model.lap,
        altNames: model.altNames,
        adaptability: model.adaptability,
        affectionLevel: model.affectionLevel,
        childFriendly: model.childFriendly,
        catFriendly: model.catFriendly,
        dogFriendly: model.dogFriendly,
        energyLevel: model.energyLevel,
        grooming: model.grooming,
        healthIssues: model.healthIssues,
        intelligence: model.intelligence,
        sheddingLevel: model.sheddingLevel,
        socialNeeds: model.socialNeeds,
        strangerFriendly: model.strangerFriendly,
        vocalisation: model.vocalisation,
        experimental: model.experimental,
        hairless: model.hairless,
        natural: model.natural,
        rare: model.rare,
        rex: model.rex,
        suppressedTail: model.suppressedTail,
        shortLegs: model.shortLegs,
        wikipediaUrl: model.wikipediaUrl,
        hypoallergenic: model.hypoallergenic,
        referenceImageId: model.referenceImageId,
        weightTable: CatWeightTable.fromModel(model.weightModel!),
      );
}
