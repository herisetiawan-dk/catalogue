
import 'package:catalogue/common/constants/local_database_type_constants.dart';
import 'package:catalogue/data/models/cat_weight_model.dart';
import 'package:hive/hive.dart';

part 'cat_weight_table.g.dart';

@HiveType(typeId: LocalDatabaseTypeConstants.catWeightTableId)
class CatWeightTable extends CatWeightModel {
  CatWeightTable({super.imperial, super.metric});

  CatWeightTable.fromModel(CatWeightModel model) {
    imperial = model.imperial;
    metric = model.metric;
  }
}