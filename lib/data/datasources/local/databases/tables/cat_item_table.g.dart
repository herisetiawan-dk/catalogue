// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cat_item_table.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class CatItemTableAdapter extends TypeAdapter<CatItemTable> {
  @override
  final int typeId = 0;

  @override
  CatItemTable read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return CatItemTable(
      id: fields[1] as String?,
      url: fields[2] as String?,
      width: fields[3] as int?,
      height: fields[4] as int?,
      breedsTable: (fields[5] as List?)?.cast<CatBreedTable>(),
    );
  }

  @override
  void write(BinaryWriter writer, CatItemTable obj) {
    writer
      ..writeByte(5)
      ..writeByte(5)
      ..write(obj.breedsTable)
      ..writeByte(1)
      ..write(obj.id)
      ..writeByte(2)
      ..write(obj.url)
      ..writeByte(3)
      ..write(obj.width)
      ..writeByte(4)
      ..write(obj.height);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CatItemTableAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
