// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cat_breed_table.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class CatBreedTableAdapter extends TypeAdapter<CatBreedTable> {
  @override
  final int typeId = 1;

  @override
  CatBreedTable read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return CatBreedTable(
      id: fields[1] as String?,
      name: fields[2] as String?,
      cfaUrl: fields[3] as String?,
      vetstreetUrl: fields[4] as String?,
      vcahospitalsUrl: fields[5] as String?,
      temperament: fields[6] as String?,
      origin: fields[7] as String?,
      countryCodes: fields[8] as String?,
      countryCode: fields[9] as String?,
      description: fields[10] as String?,
      lifeSpan: fields[11] as String?,
      indoor: fields[12] as int?,
      lap: fields[13] as int?,
      altNames: fields[14] as String?,
      adaptability: fields[15] as int?,
      affectionLevel: fields[16] as int?,
      childFriendly: fields[17] as int?,
      catFriendly: fields[18] as int?,
      dogFriendly: fields[19] as int?,
      energyLevel: fields[20] as int?,
      grooming: fields[21] as int?,
      healthIssues: fields[22] as int?,
      intelligence: fields[23] as int?,
      sheddingLevel: fields[24] as int?,
      socialNeeds: fields[25] as int?,
      strangerFriendly: fields[26] as int?,
      vocalisation: fields[27] as int?,
      experimental: fields[28] as int?,
      hairless: fields[29] as int?,
      natural: fields[30] as int?,
      rare: fields[31] as int?,
      rex: fields[32] as int?,
      suppressedTail: fields[33] as int?,
      shortLegs: fields[34] as int?,
      wikipediaUrl: fields[35] as String?,
      hypoallergenic: fields[36] as int?,
      referenceImageId: fields[37] as String?,
      weightTable: fields[38] as CatWeightTable?,
    );
  }

  @override
  void write(BinaryWriter writer, CatBreedTable obj) {
    writer
      ..writeByte(38)
      ..writeByte(38)
      ..write(obj.weightTable)
      ..writeByte(1)
      ..write(obj.id)
      ..writeByte(2)
      ..write(obj.name)
      ..writeByte(3)
      ..write(obj.cfaUrl)
      ..writeByte(4)
      ..write(obj.vetstreetUrl)
      ..writeByte(5)
      ..write(obj.vcahospitalsUrl)
      ..writeByte(6)
      ..write(obj.temperament)
      ..writeByte(7)
      ..write(obj.origin)
      ..writeByte(8)
      ..write(obj.countryCodes)
      ..writeByte(9)
      ..write(obj.countryCode)
      ..writeByte(10)
      ..write(obj.description)
      ..writeByte(11)
      ..write(obj.lifeSpan)
      ..writeByte(12)
      ..write(obj.indoor)
      ..writeByte(13)
      ..write(obj.lap)
      ..writeByte(14)
      ..write(obj.altNames)
      ..writeByte(15)
      ..write(obj.adaptability)
      ..writeByte(16)
      ..write(obj.affectionLevel)
      ..writeByte(17)
      ..write(obj.childFriendly)
      ..writeByte(18)
      ..write(obj.catFriendly)
      ..writeByte(19)
      ..write(obj.dogFriendly)
      ..writeByte(20)
      ..write(obj.energyLevel)
      ..writeByte(21)
      ..write(obj.grooming)
      ..writeByte(22)
      ..write(obj.healthIssues)
      ..writeByte(23)
      ..write(obj.intelligence)
      ..writeByte(24)
      ..write(obj.sheddingLevel)
      ..writeByte(25)
      ..write(obj.socialNeeds)
      ..writeByte(26)
      ..write(obj.strangerFriendly)
      ..writeByte(27)
      ..write(obj.vocalisation)
      ..writeByte(28)
      ..write(obj.experimental)
      ..writeByte(29)
      ..write(obj.hairless)
      ..writeByte(30)
      ..write(obj.natural)
      ..writeByte(31)
      ..write(obj.rare)
      ..writeByte(32)
      ..write(obj.rex)
      ..writeByte(33)
      ..write(obj.suppressedTail)
      ..writeByte(34)
      ..write(obj.shortLegs)
      ..writeByte(35)
      ..write(obj.wikipediaUrl)
      ..writeByte(36)
      ..write(obj.hypoallergenic)
      ..writeByte(37)
      ..write(obj.referenceImageId);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CatBreedTableAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
