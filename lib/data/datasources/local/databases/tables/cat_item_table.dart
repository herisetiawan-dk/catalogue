import 'package:catalogue/common/constants/local_database_type_constants.dart';
import 'package:catalogue/data/datasources/local/databases/tables/cat_breed_table.dart';
import 'package:catalogue/data/models/cat_breed_model.dart';
import 'package:hive/hive.dart';
import 'package:catalogue/data/models/cat_item_model.dart';

part 'cat_item_table.g.dart';

@HiveType(typeId: LocalDatabaseTypeConstants.catTableId)
class CatItemTable extends CatItemModel {
  @override
  @HiveField(5)
  List<CatBreedTable>? breedsTable;

  CatItemTable({
    super.id,
    super.url,
    super.width,
    super.height,
    this.breedsTable,
  }) : super(breeds: breedsTable);

  static CatItemTable fromModel(CatItemModel model) => CatItemTable(
        id: model.id,
        url: model.url,
        width: model.width,
        height: model.height,
        breedsTable: model.breedsModel?.map(CatBreedTable.fromModel).toList(),
      );
}
