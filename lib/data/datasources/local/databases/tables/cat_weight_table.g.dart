// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cat_weight_table.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class CatWeightTableAdapter extends TypeAdapter<CatWeightTable> {
  @override
  final int typeId = 2;

  @override
  CatWeightTable read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return CatWeightTable(
      imperial: fields[1] as String?,
      metric: fields[2] as String?,
    );
  }

  @override
  void write(BinaryWriter writer, CatWeightTable obj) {
    writer
      ..writeByte(2)
      ..writeByte(1)
      ..write(obj.imperial)
      ..writeByte(2)
      ..write(obj.metric);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CatWeightTableAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
