import 'package:catalogue/common/constants/local_database_boxname_constants.dart';
import 'package:catalogue/data/datasources/local/base_local_datasource.dart';
import 'package:catalogue/data/datasources/local/databases/tables/cat_breed_table.dart';
import 'package:catalogue/data/datasources/local/databases/tables/cat_item_table.dart';
import 'package:catalogue/data/datasources/local/databases/tables/cat_weight_table.dart';
import 'package:catalogue/data/datasources/local/utils/database_utils.dart';
import 'package:catalogue/data/models/cat_item_model.dart';

class CatLocalDatasource
    extends BaseLocalDatasource<CatItemTable, CatItemModel> {
  CatLocalDatasource()
      : super(
          boxName: LocalDatabaseBoxnameConstants.catBoxname,
        ) {
    DatabaseUtil.registerAdapter<CatItemTable>(CatItemTableAdapter());
    DatabaseUtil.registerAdapter<CatBreedTable>(CatBreedTableAdapter());
    DatabaseUtil.registerAdapter<CatWeightTable>(CatWeightTableAdapter());
  }

  @override
  Future<List<CatItemModel>> getFormattedData() async {
    final List<CatItemTable> cats = await getAll();
    return cats;
  }

  @override
  Future<void> insertOrUpdateItems(List<CatItemModel> items) async {
    await deleteAll();
    final Map<String, CatItemTable> catMap = {
      for (CatItemModel cat in items) cat.id!: CatItemTable.fromModel(cat)
    };

    await putAll(catMap);
  }
}
