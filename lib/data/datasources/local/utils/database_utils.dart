import 'dart:io';

import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart';

class DatabaseUtil {
  static Future<void> initDatabase() async {
    final Directory directory = await getApplicationDocumentsDirectory();
    Hive.init(directory.path);
  }

  static void registerAdapter<T>(TypeAdapter<T> adapter) {
    if (!Hive.isAdapterRegistered(adapter.typeId)) {
      Hive.registerAdapter<T>(adapter);
    }
  }

  static Future<Box<Type>> getBox<Type>({
    required String boxName,
  }) async {
    if (Hive.isBoxOpen(boxName)) return Hive.box(boxName);

    return Hive.openBox(boxName);
  }
}
