import 'dart:async';

import 'package:catalogue/data/datasources/local/cat_local_datasource.dart';
import 'package:catalogue/data/datasources/remote/cat_remote_datasource.dart';
import 'package:catalogue/data/models/cat_item_model.dart';
import 'package:catalogue/domain/repositories/cat_repository.dart';

class CatRepositoryImpl extends CatRepository {
  final CatRemoteDatasource catRemoteDatasource;
  final CatLocalDatasource catLocalDatasource;

  CatRepositoryImpl({
    required this.catRemoteDatasource,
    required this.catLocalDatasource,
  });

  @override
  Future<CatListModel> getCats([bool forceRemote = false]) async {
    final localCats = await catLocalDatasource.getFormattedData();

    if (localCats.isEmpty || forceRemote) {
      final remoteCats = await catRemoteDatasource.getCats();
      unawaited(catLocalDatasource.insertOrUpdateItems(remoteCats));
      return remoteCats;
    }

    return localCats;
  }

  @override
  Future<CatItemModel?> getCat(String id) async {
    final cat = await catLocalDatasource.get(id);

    return cat;
  }


}
