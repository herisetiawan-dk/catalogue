import 'package:catalogue/common/cubit/loading_cubit.dart';
import 'package:catalogue/domain/entities/cat_item_entity.dart';
import 'package:catalogue/domain/usecases/cat_usecase.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CatDetailCubit extends Cubit<CatItemEntity?> {
  final LoadingCubit loadingCubit;
  final CatUseCase catUseCase;

  CatDetailCubit({
    required this.loadingCubit,
    required this.catUseCase,
  }) : super(null);

  void get(String id) async {
    loadingCubit.startLoading();

    final cat = await catUseCase.getCat(id);
    emit(cat);

    loadingCubit.stopLoading();
  }
}
