import 'package:catalogue/common/cubit/loading_cubit.dart';
import 'package:catalogue/domain/entities/cat_item_entity.dart';
import 'package:catalogue/domain/usecases/cat_usecase.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CatListCubit extends Cubit<CatListState> {
  final LoadingCubit loadingCubit;
  final CatUseCase catUseCase;

  CatListCubit({
    required this.loadingCubit,
    required this.catUseCase,
  }) : super(InitialCatListState());

  void getAll() async {
    loadingCubit.startLoading();

    emit(LoadingCatListState());

    try {
      final cats = await catUseCase.getCats();
      emit(LoadedCatListState(cats: cats));
    } catch (e) {
      emit(ErrorCatListState());
    }

    loadingCubit.stopLoading();
  }

  void refresh() async {
    loadingCubit.startLoading();

    emit(LoadingCatListState());
    try {
      final cats = await catUseCase.getCats(true);
      emit(LoadedCatListState(cats: cats));
    } catch (e) {
      emit(ErrorCatListState());
    }

    loadingCubit.stopLoading();
  }
}

abstract class CatListState {
  final CatListEntity cats;

  CatListState({this.cats = const []});
}

class InitialCatListState extends CatListState {}

class LoadingCatListState extends CatListState {}

class LoadedCatListState extends CatListState {
  LoadedCatListState({required super.cats});
}

class ErrorCatListState extends CatListState {}
