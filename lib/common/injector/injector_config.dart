import 'package:catalogue/common/cubit/cat_detail_cubit.dart';
import 'package:catalogue/common/cubit/cat_list_cubit.dart';
import 'package:catalogue/common/cubit/loading_cubit.dart';
import 'package:catalogue/data/datasources/local/cat_local_datasource.dart';
import 'package:catalogue/data/datasources/remote/cat_remote_datasource.dart';
import 'package:catalogue/data/repositories/cat_repository_impl.dart';
import 'package:catalogue/domain/repositories/cat_repository.dart';
import 'package:catalogue/domain/usecases/cat_usecase.dart';
import 'package:kiwi/kiwi.dart';

part 'injector_config.g.dart';

abstract class InjectorConfig {
  static void setup() {
      final injector = _$InjectorConfig();
      injector._configure();
  }

  @Register.singleton(LoadingCubit)
  @Register.singleton(CatListCubit)
  @Register.factory(CatDetailCubit)
  @Register.singleton(CatUseCase)
  @Register.singleton(CatRepository, from: CatRepositoryImpl)
  @Register.singleton(CatLocalDatasource)
  @Register.singleton(CatRemoteDatasource)
  void _configure();
}
