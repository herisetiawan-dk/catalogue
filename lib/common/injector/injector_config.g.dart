// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'injector_config.dart';

// **************************************************************************
// KiwiInjectorGenerator
// **************************************************************************

class _$InjectorConfig extends InjectorConfig {
  @override
  void _configure() {
    final KiwiContainer container = KiwiContainer();
    container
      ..registerSingleton((c) => LoadingCubit())
      ..registerSingleton((c) => CatListCubit(
          loadingCubit: c<LoadingCubit>(), catUseCase: c<CatUseCase>()))
      ..registerFactory((c) => CatDetailCubit(
          loadingCubit: c<LoadingCubit>(), catUseCase: c<CatUseCase>()))
      ..registerSingleton((c) => CatUseCase(catRepository: c<CatRepository>()))
      ..registerSingleton<CatRepository>((c) => CatRepositoryImpl(
          catRemoteDatasource: c<CatRemoteDatasource>(),
          catLocalDatasource: c<CatLocalDatasource>()))
      ..registerSingleton((c) => CatLocalDatasource())
      ..registerSingleton((c) => CatRemoteDatasource());
  }
}
