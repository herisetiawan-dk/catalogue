import 'package:catalogue/common/injector/injector_config.dart';
import 'package:catalogue/data/datasources/local/utils/database_utils.dart';
import 'package:catalogue/presentation/app.dart';
import 'package:flutter/material.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await DatabaseUtil.initDatabase();
  InjectorConfig.setup();

  runApp(const App());
}