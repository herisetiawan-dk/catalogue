import 'package:catalogue/common/cubit/cat_list_cubit.dart';
import 'package:catalogue/common/cubit/loading_cubit.dart';
import 'package:catalogue/common/injector/injector.dart';
import 'package:catalogue/presentation/screen/cat_dashboard/widgets/cat_box_container.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CatDashboardScreen extends StatelessWidget {
  final CatListCubit catListCubit = Injector.resolve<CatListCubit>();
  final LoadingCubit loadingCubit = Injector.resolve<LoadingCubit>();

  CatDashboardScreen({super.key}) {
    catListCubit.getAll();
  }

  final ScrollController controller = ScrollController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('[Cat]alogue'),
        actions: [
          IconButton(
            onPressed: () {
              if (!loadingCubit.state) catListCubit.refresh();
            },
            icon: const Icon(Icons.refresh),
          ),
        ],
      ),
      body: BlocProvider<CatListCubit>(
        create: (context) => catListCubit,
        child: BlocBuilder<CatListCubit, CatListState>(
          builder: (context, state) {
            if (state is LoadedCatListState) {
              return GridView.count(
                crossAxisCount: 2,
                children:
                state.cats.map((cat) => CatBoxContainer(cat: cat)).toList(),
              );
            } else if (state is ErrorCatListState) {
              return const Center(
                child: Text('Failed to load :('),
              );
            }
            return Container();
          },
        ),
      ),
    );
  }
}
