import 'package:catalogue/common/constants/route_constants.dart';
import 'package:catalogue/domain/entities/cat_item_entity.dart';
import 'package:catalogue/presentation/screen/cat_detail/cat_detail_screen.dart';
import 'package:flutter/material.dart';

class CatBoxContainer extends StatelessWidget {
  final CatItemEntity cat;

  const CatBoxContainer({super.key, required this.cat});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.pushNamed(
        context,
        RouteConstants.detail,
        arguments: CatDetailArguments(cat.id!),
      ),
      child: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: NetworkImage(cat.url!),
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }
}
