import 'package:catalogue/common/cubit/cat_detail_cubit.dart';
import 'package:catalogue/common/injector/injector.dart';
import 'package:catalogue/domain/entities/cat_item_entity.dart';
import 'package:catalogue/presentation/widget/drawer_container.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CatDetailArguments {
  final String id;

  CatDetailArguments(this.id);
}

class CatDetailScreen extends StatefulWidget {
  const CatDetailScreen({super.key});

  @override
  State<CatDetailScreen> createState() => _CatDetailScreenState();
}

class _CatDetailScreenState extends State<CatDetailScreen> {
  late final CatDetailCubit catDetailCubit;

  @override
  void initState() {
    catDetailCubit = Injector.resolve<CatDetailCubit>();
    super.initState();
  }

  @override
  void dispose() {
    catDetailCubit.close();
    super.dispose();
  }

  CatDetailArguments get arguments =>
      ModalRoute.of(context)!.settings.arguments as CatDetailArguments;

  @override
  Widget build(BuildContext context) {
    catDetailCubit.get(arguments.id);
    return Scaffold(
      body: BlocBuilder<CatDetailCubit, CatItemEntity?>(
        bloc: catDetailCubit,
        builder: (context, cat) {
          if (cat == null) return Container();
          return SizedBox(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: Stack(
              children: [
                Container(
                  height: MediaQuery.of(context).size.width,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: NetworkImage(cat.url!),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Column(
                  children: [
                    SizedBox(
                      height: MediaQuery.of(context).size.width - 16,
                    ),
                    Expanded(
                      child: DrawerContainer(
                        children: [
                          ...(cat.breeds ?? []).map((breed) {
                            return SizedBox(
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Text(
                                    breed.name!,
                                    style: const TextStyle(
                                      fontSize: 24,
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                  Text(
                                    breed.origin!,
                                    style: const TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w200,
                                    ),
                                  ),
                                  Text(
                                    breed.description!,
                                    textAlign: TextAlign.center,
                                    style: const TextStyle(
                                      fontWeight: FontWeight.w300,
                                    ),
                                  ),
                                ],
                              ),
                            );
                          }),
                          const Spacer(),
                          Column(
                            mainAxisSize: MainAxisSize.min,
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: [
                              SizedBox(
                                height: 50,
                                child: ElevatedButton(
                                  onPressed: Navigator.of(context).pop,
                                  child: const Text('Back'),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
