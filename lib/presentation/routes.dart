import 'package:catalogue/common/constants/route_constants.dart';
import 'package:catalogue/presentation/screen/cat_dashboard/cat_dashboard_screen.dart';
import 'package:catalogue/presentation/screen/cat_detail/cat_detail_screen.dart';
import 'package:flutter/material.dart';

class Routes {
  static Map<String, WidgetBuilder> getAll() => {
    RouteConstants.dashboard: (context) => CatDashboardScreen(),
    RouteConstants.detail: (context) => const CatDetailScreen(),
  };
}