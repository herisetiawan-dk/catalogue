import 'package:flutter/material.dart';

class DrawerContainer extends StatelessWidget {
  final List<Widget> children;

  const DrawerContainer({
    super.key,
    required this.children,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: const EdgeInsets.all(16).copyWith(bottom: 24),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(16),
      ),
      child: Column(
        children: [
          Container(
            decoration: BoxDecoration(
              color: Colors.grey,
              borderRadius: BorderRadius.circular(16),
            ),
            width: 42,
            height: 4,
          ),
          const SizedBox(
            height: 16,
          ),
          ...children,
        ],
      ),
    );
  }
}
