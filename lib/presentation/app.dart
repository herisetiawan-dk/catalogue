import 'package:catalogue/common/constants/route_constants.dart';
import 'package:catalogue/common/cubit/loading_cubit.dart';
import 'package:catalogue/common/injector/injector.dart';
import 'package:catalogue/presentation/routes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class App extends StatelessWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.amber),
        useMaterial3: true,
      ),
      builder: (context, child) {
        return BlocBuilder<LoadingCubit, bool>(
          bloc: Injector.resolve<LoadingCubit>(),
          builder: (context, loading) {
            return Stack(
              children: [
                child!,
                Visibility(
                  visible: loading,
                  child: Container(
                    color: Colors.black.withOpacity(0.1),
                    child: const Center(
                      child: CircularProgressIndicator(),
                    ),
                  )
                ),
              ],
            );
          },
        );
      },
      routes: Routes.getAll(),
      initialRoute: RouteConstants.dashboard,
    );
  }
}
